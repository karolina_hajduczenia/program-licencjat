import sys
from math import pi, sin, cos, tan, sqrt, acos
from datetime import timezone, datetime
import matplotlib.pyplot as plt

T = 365.2425*24*3600
e = 0.0167086
a = 1.000001018 # w AU

alfa = pi*23.44/180 # kąt nachylenia do ekliptyki w radianach
T0 = 86164.091 # doba gwiazdowa 23h 56m 4s

eps = sys.float_info.epsilon

dt = datetime(1988, 1, 4, 0, 58)
t0 = dt.replace(tzinfo=timezone.utc).timestamp()

b = a*((1-e**2)**(1/2))

def po_elipsie(t):

    M = (2*pi/T)*(t-t0)
    M %= 2*pi

    E0 = 0
    E1 = M + e*sin(E0)

    i=0
    while abs(E1-E0) >= eps and i<20:
        E0 = E1
        E1 = M + e*sin(E0)
        i += 1

    wsp_x = a*(cos(E1) - e)
    wsp_y = b*sin(E1)

    return [wsp_x, wsp_y, 0]

def obroty(t, przesilenie, fi):

    D = (2*pi/T0)*(t-t0+7108)
    D %= 2*pi
    x = przesilenie[0]
    y = przesilenie[1]
    
    u = (x*sin(alfa), y*sin(alfa), cos(alfa))
    u3 = (-y, x, 0.0)
    u2 = (x/(1+(tan(alfa))**2)**(1/2), y/(1+(tan(alfa))**2)**(1/2), -tan(alfa)/(1+(tan(alfa))**2)**(1/2))
    
    v = []
    for i in range(len(u)):
        k = sin(fi)*u[i] + cos(fi)*cos(D)*u2[i] + cos(fi)*sin(D)*u3[i]
        v.append(k)

    return v

def t_zamiana(year, month, day, hour, m):
    d = datetime(year, month, day, hour, m)
    return d.replace(tzinfo=timezone.utc).timestamp()

def skalar(v1, v2):
    return sum((a*b) for a, b in zip(v1, v2))

def dl(v):
    return sqrt(skalar(v, v))

def kat(v1, v2):
    return acos(skalar(v1, v2)/(dl(v1)*dl(v2)))

def normowanie(w):
    return [wsp/dl(w) for wsp in w]

def wyszukiwanie(skalary, godziny):
    polowa = godziny.index([12, 0])
    przed = skalary[0:polowa]
    po = skalary[polowa:]
    wschod = godziny[skalary.index(min(przed))]
    zachod = godziny[skalary.index(min(po))]
    return [wschod, zachod]

def letni(godziny):
    for i in range(3): godziny[i][0] += 1
    return godziny

def zamiana(month, day, godziny):
    if (month>3 and month<10):
        godziny = letni(godziny)
    if (month==3 and day>=27):
        godziny = letni(godziny)
    if (month==10 and day<=24):
        godziny = letni(godziny)
    
    return godziny

przesilenie = po_elipsie(t_zamiana(1987, 12, 22, 10, 45))

def poludnie(year, month, day, szer):
    tp0 = t_zamiana(year, month, day, 0, 0)
    maks_kat = kat(po_elipsie(tp0), obroty(tp0, przesilenie, szer))
    godz_pol = []

    for i in range(0, 24):
        for j in range(0, 60):
            t = t_zamiana(year, month, day, i, j)
            wsp_elipsa = po_elipsie(t)
            wsp_miasta = obroty(t, przesilenie, szer)

            if kat(wsp_elipsa, wsp_miasta) > maks_kat:
                maks_kat = kat(wsp_elipsa, wsp_miasta)
                godz_pol = [i, j]

    return godz_pol

def wschod_zachod(year, month, day, szer):
    proste = []
    l1 = []

    for i in range(0, 24):
        for j in range(0, 60):
            t = t_zamiana(year, month, day, i, j)
            wsp_elipsa = normowanie(po_elipsie(t))
            wsp_miasta = normowanie(obroty(t, przesilenie, szer))
            proste.append(abs(skalar(wsp_elipsa, wsp_miasta)-cos(89.15*pi/180)))
            l1.append([i,j])

    godziny = wyszukiwanie(proste, l1)
    return godziny

def dni_noce(year, month, day, szer):
    szer = szer*pi/180
    godz_pol = poludnie(year, month, day, szer)
    godziny = wschod_zachod(year, month, day, szer)
    
    godziny.append(godz_pol)
    godziny = zamiana(month, day, godziny)
    co = ["wschód", "zachód", "południe słoneczne"]
    return list(zip(co, godziny))

def dlugosc_dnia(year, month, day, szer):
    godz = wschod_zachod(year, month, day, szer)
    wsch = godz[0]
    zach = godz[1]
    wsch_w_sek = t_zamiana(year, month, day, wsch[0], wsch[1])
    zach_w_sek = t_zamiana(year, month, day, zach[0], zach[1])
    sek = zach_w_sek - wsch_w_sek
    return sek/3600

def poludnie_roczne(szer):
    szer = szer*pi/180
    godzina_poludnia = []
    y= []
    dni = []
    i = 1
    while i<13:
        if (i%2==1 and i<8) or (i%2==0 and i>7):
            for j in range(1, 32):
                x = poludnie(2022, i, j, szer)
                y.append(str(x[0])+':'+str(x[1]))
                godzina_poludnia.append(x[0]+x[1]/60)
                dni.append(i+(j-1)/31)
        elif i==2:
            for j in range(1, 29):
                x = poludnie(2022, i, j, szer)
                godzina_poludnia.append(x[0]+x[1]/60)
                dni.append(i+(j-1)/28)
        else:
            for j in range(1, 31):
                x = poludnie(2022, i, j, szer)
                godzina_poludnia.append(x[0]+x[1]/60)
                dni.append(i+(j-1)/30)
        i = i+1

    return dni, godzina_poludnia

def caly_rok(szer):
    szer = szer*pi/180
    trwanie_dnia = []
    dni = []
    i = 1
    while i<13:
        if (i%2==1 and i<8) or (i%2==0 and i>7):
            for j in range(1, 32):
                trwanie_dnia.append(dlugosc_dnia(2022, i, j, szer))
                dni.append(i+(j-1)/31)
        elif i==2:
            for j in range(1, 29):
                trwanie_dnia.append(dlugosc_dnia(2022, i, j, szer))
                dni.append(i+(j-1)/28)
        else:
            for j in range(1, 31):
                trwanie_dnia.append(dlugosc_dnia(2022, i, j, szer))
                dni.append(i+(j-1)/30)
        i = i+1

    return dni, trwanie_dnia

def wykres1(szer):
    ax = plt.figure().add_subplot()
    ax.plot(*caly_rok(szer), label='fi=30')
    ax.legend(fontsize="small")
    plt.title("Długość dnia w roku")
    plt.show()

def wykres2():
    ax = plt.figure().add_subplot()
    ax.plot(*caly_rok(60), label='fi=60N')
    ax.plot(*caly_rok(51.1), label='fi=51.1N')
    ax.plot(*caly_rok(30), label='fi=30N')
    ax.plot(*caly_rok(0), label='fi=0')
    ax.plot(*caly_rok(-30), label='fi=30S')
    ax.plot(*caly_rok(-45), label='fi=45S')
    ax.plot(*caly_rok(-60), label='fi=60S')
    ax.legend(fontsize="xx-small")
    plt.title("Zmiana długości dnia w roku")
    plt.xlabel("Miesiąc")
    plt.ylabel("Długość dnia w h")
    plt.grid(axis="y")
    plt.show()

def wykres3(szer):
    ax = plt.figure().add_subplot()
    ax.plot(*poludnie_roczne(szer))
    plt.title("Południe słoneczne we Wrocławiu")
    plt.xlabel("Miesiąc")
    plt.ylabel("Godzina")
    plt.grid(axis="y")
    plt.show()